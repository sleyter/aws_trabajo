package aws.mitocode.spring.controller.api;

import aws.mitocode.spring.dto.RespuestaApi;
import aws.mitocode.spring.model.Votante;
import aws.mitocode.spring.service.IVotanteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("api/votante")
@Slf4j
public class VotanteController {

  @Autowired
  private IVotanteService votanteService;

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @GetMapping(value="listar", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<Votante>> obtenerTodos(Pageable pageable){
    try {
      return new ResponseEntity<List<Votante>>(
              votanteService.findAll(), HttpStatus.OK);
    }catch(Exception e) {
      log.error("Error: ",e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping(value="registrar", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<RespuestaApi> guardar(
          @RequestBody Votante votante){
    try {
      votanteService.save(votante);
      return new ResponseEntity<RespuestaApi>(new RespuestaApi("OK",""), HttpStatus.OK);
    }catch(Exception e) {
      log.error("Error: ",e);
      return new ResponseEntity<RespuestaApi>(new RespuestaApi("",""), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @PostMapping(value="actualizar", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<RespuestaApi> actualizar(
          @RequestBody Votante votante){
    try {
      votanteService.update(votante);
      return new ResponseEntity<RespuestaApi>(new RespuestaApi("OK",""), HttpStatus.OK);
    }catch(Exception e) {
      log.error("Error: ",e);
      return new ResponseEntity<RespuestaApi>(new RespuestaApi("",""), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @DeleteMapping(value="eliminar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<RespuestaApi> eliminar(
          @PathVariable int id){
    try {
      votanteService.delete(id);
      return new ResponseEntity<RespuestaApi>(new RespuestaApi("OK",""), HttpStatus.OK);
    }catch(Exception e) {
      log.error("Error: ",e);
      return new ResponseEntity<RespuestaApi>(new RespuestaApi("",""), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
