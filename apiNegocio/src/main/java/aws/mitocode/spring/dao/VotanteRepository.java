package aws.mitocode.spring.dao;

import aws.mitocode.spring.model.Votante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VotanteRepository extends JpaRepository<Votante, Integer> {
}
