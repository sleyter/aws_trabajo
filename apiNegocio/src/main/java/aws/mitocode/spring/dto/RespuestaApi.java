package aws.mitocode.spring.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RespuestaApi{

	private String status;
	private Object body;
	
	public RespuestaApi() {}
	
	public RespuestaApi(String status, Object body) {
		this.status = status;
		this.body = body;
	}
}
