package aws.mitocode.spring.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "votantes")
@Getter
@Setter
public class Votante implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column(name = "nombres", length = 200, nullable = false)
  private String nombres;

  @Column(name = "apellidos", length = 200, nullable = false)
  private String apellidos;

  @Column(name = "edad", nullable = false)
  private Integer edad;

  @Column(name = "voto", length = 100, nullable = false)
  private String voto;

}
