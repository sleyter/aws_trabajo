package aws.mitocode.spring.service;

import aws.mitocode.spring.model.Votante;

import java.util.List;

public interface IVotanteService {

  List<Votante> findAll();

  Votante save(Votante votante);

  Votante update(Votante votante);

  void delete(Integer id);

}
