package aws.mitocode.spring.service.impl;

import aws.mitocode.spring.dao.VotanteRepository;
import aws.mitocode.spring.model.Votante;
import aws.mitocode.spring.service.IVotanteService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VotanteService implements IVotanteService {

  private final VotanteRepository votanteRepository;

  public VotanteService(VotanteRepository votanteRepository) {
    this.votanteRepository = votanteRepository;
  }

  @Override
  public List<Votante> findAll() {
    return votanteRepository.findAll();
  }

  @Override
  public Votante save(Votante votante) {
    return votanteRepository.save(votante);
  }

  @Override
  public Votante update(Votante votante) {
    Votante votanteDB = votanteRepository.findOne(votante.getId());
    votanteDB.setNombres(votante.getNombres());
    votanteDB.setApellidos(votante.getApellidos());
    votanteDB.setEdad(votante.getEdad());
    votanteDB.setVoto(votante.getVoto());
    return votanteRepository.save(votanteDB);
  }

  @Override
  public void delete(Integer id) {
    votanteRepository.delete(id);
  }

}
