export class Votacion{
  id: number;
  nombres: String;
  apellidos: String;
  edad: number;
  voto: String;

  public cleanVotacion(){
    this.id = null;
    this.nombres = '';
    this.apellidos = '';
    this.edad = 0;
    this.voto = '';
  }
}
