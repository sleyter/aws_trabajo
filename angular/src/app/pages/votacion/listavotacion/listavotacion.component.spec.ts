import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListavotacionComponent } from './listavotacion.component';

describe('ListavotacionComponent', () => {
  let component: ListavotacionComponent;
  let fixture: ComponentFixture<ListavotacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListavotacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListavotacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
