import { Component, OnInit } from '@angular/core';
import { Votacion } from 'src/app/_model/Votacion';
import { VotacionService } from 'src/app/_services/votacion.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { VotacionComponent } from '../votacion.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listavotacion',
  templateUrl: './listavotacion.component.html',
  styleUrls: ['./listavotacion.component.css']
})
export class ListavotacionComponent implements OnInit {

  data: Votacion[] = [];
  displayedColumns: string[] = ['id', 'nombres', 'apellidos', 'edad', 'voto', 'acciones'];

  constructor(private votacionService: VotacionService,
    private _snackBar: MatSnackBar,
    private router: Router) { }

  ngOnInit() {
    this.votacionService.obtenerTodosLosRegistros().subscribe(resp => {
      this.data = resp;
    });
  }

  actualizarVoto(voto: Votacion) {
    this.votacionService.row = voto;
    this.router.navigate(['/app/votacion']);
  }

  eliminarVoto(id: number) {
    this.votacionService.eliminarVotacion(id).subscribe(resp => {
      this._snackBar.open('Votación Eliminada', 'OK', {
        duration: 2000
      });
      this.data = this.data.filter( e => e.id !== id);
    });
  }

}
