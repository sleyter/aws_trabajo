import { Component, OnInit, Inject } from '@angular/core';
import { Votacion } from 'src/app/_model/Votacion';
import { VotacionService } from 'src/app/_services/votacion.service';
import { RespuestaApi } from 'src/app/_model/RespuestaApi';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-votacion',
  templateUrl: './votacion.component.html',
  styleUrls: ['./votacion.component.css']
})
export class VotacionComponent implements OnInit {

  votacion: Votacion = new Votacion();

  constructor(private votacionService: VotacionService,
    private _snackBar: MatSnackBar) { }

  ngOnInit() {
    this.votacion.cleanVotacion();
    if (this.votacionService.row != null) {
      this.votacion = this.votacionService.row;
    }
  }

  guardarVotacion() {
    this.votacionService.guardarVotacion(this.votacion).subscribe(resp => {
      this._snackBar.open('Votación Guarda','OK',{
        duration: 2000
      });
      this.votacion.cleanVotacion();
    });
  }

  actualizarVotacion(){
    this.votacionService.actualizarVotacion(this.votacion).subscribe(resp => {
      this._snackBar.open('Votación Actulizada','OK',{
        duration: 2000
      });
      this.votacion.cleanVotacion();
    });
  }


}
