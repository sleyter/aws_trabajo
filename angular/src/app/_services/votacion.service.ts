import { Injectable } from '@angular/core';
import { HOST_BACKEND } from '../_shared/constants';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Votacion } from '../_model/Votacion';

@Injectable({
  providedIn: 'root'
})
export class VotacionService {

  urlVotacion: string = `${HOST_BACKEND}/api/votante`;

  mensajeRegistro = new Subject<string>();

  row: Votacion;

  constructor(private httpClient: HttpClient) { }

  obtenerTodosLosRegistros() {
    return this.httpClient.get<Votacion[]>(`${this.urlVotacion}/listar`);
  }

  guardarVotacion(votacion: Votacion) {
    return this.httpClient.post(`${this.urlVotacion}/registrar`, votacion);
  }

  actualizarVotacion(votacion: Votacion) {
    return this.httpClient.post(`${this.urlVotacion}/actualizar`, votacion);
  }

  eliminarVotacion(id: number) {
    return this.httpClient.delete(`${this.urlVotacion}/eliminar/${id}`);
  }

}
