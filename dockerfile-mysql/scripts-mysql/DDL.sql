CREATE DATABASE IF NOT EXISTS encuestaDB CHARACTER SET latin1 COLLATE latin1_swedish_ci;

USE encuestaDB;

SET foreign_key_checks = 0;

DROP TABLE IF EXISTS votantes;

SET foreign_key_checks = 1;

CREATE TABLE votantes(
   	id INT(4) NOT NULL AUTO_INCREMENT,
	nombres VARCHAR(200) NOT NULL,
    apellidos VARCHAR(200) NOT NULL,
    voto VARCHAR(100) NOT NULL,
    edad INT(4) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=INNODB;
